
def returns_tuple_of_three(a,b,c):
    return (a,b,c)

def insertionBaseDonne(connexion, motsLigne, motsColonnes, fenetre):
    cur = connexion.cursor()
    print('Depart insertion BaseDonne')

    listTMP = list()
    tuple(listTMP)

    for i in range(len(motsLigne)):
        listTMP.append(returns_tuple_of_three(motsLigne[i],motsColonnes[i],fenetre))
        
    enonceBatch = 'INSERT INTO Matrice(id, motCentral,motConcurence,taille_fenetre) VALUES(seq_matrice.NEXTVAL, :1, :2, :3)'
    params = listTMP-1

    print('Insertion serveur')
    cur.executemany(enonceBatch, params, batcherrors=True)
    connexion.commit()
    print('Fin Insertion serveur')
    cur.close()
    print('Fin insertion BaseDonne')

def getDictionnaire(connexion):
    cur=connexion.cursor()
    cur.execute('SELECT motcentral as a from matrice union select motConcurence as a from matrice order by a')
    fc = cur.fetchall()
    fca = [row[0] for row in fc]
    return fca