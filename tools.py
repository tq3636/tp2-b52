import re

def cleaner(mot):
	mot = mot.lower()
	return re.sub('[^a-zA-ZàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ.]+', ' ', mot)
	
def cleanerPoint(mot):
	mot = mot.lower()
	return re.sub('[^a-zA-ZàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ]+', ' ', mot)
	
def countWords(texte):
	return len(texte.split())
	
def remove(tup,dict,idx_mot):
	mat = ['de','la','à','l','et','dans','que','il','les','ce','le','qu','ne','sur','son','pas','un','s','n','d','sa','ses','se','une','des','en','au','on']
	matint = []
	a_enlever = []
	
	#trouver idx des mots a enlever
	for i in range (0, len(mat)):
		matint.append(dict.trouver_index(mat[i]))
	
	#enlever mot recherché
	matint.append(idx_mot)

	#trouver s'il y as des mots a enlever
	for i in range (0, len(tup)):
		if tup[i][0] in matint:
			a_enlever.append(i)
	
	#enlever les mots
	for i in range (0, len(a_enlever)):
		tup.pop(a_enlever[i]-i)
	
	return tup