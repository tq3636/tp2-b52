import tools
import baseDonnees as bd

class dictionnaire:
	def __init__(self,connexion):
		self.connexion=connexion
		self.dict = {}
	#__init__
	
	def grandeur_dictionnaire(self):
		return len(self.dict.keys())
	
	def definir_dictionaire(self, phrases, nb_phrases):
		x=0
		for i in range (0, nb_phrases):
			mots = phrases[i].split()
		
			for j in range (0 , tools.countWords(phrases[i])):
				mot = mots[j]
				if mot not in self.dict:
					x += 1
					self.dict [mot] = x

	def definir_dico_s(self):
		liste = bd.getDictionnaire(self.connexion)
		x = 0
		for j in range (0 , len(liste)):
			mot = liste[j]
			if mot not in self.dict:
				x += 1
				self.dict [mot] = x
	
	def trouver_index(self,mot):
		idx = self.dict.get(mot)
		if idx == None:
			return idx
		return idx -1
	
	def trouver_mot(self,idx):
		idx += 1
		mots = 0
		for mot in self.dict:
			if self.dict.get(mot) == idx:
				mots=mot
		return mots
	
	def d(self):
		return self.dict