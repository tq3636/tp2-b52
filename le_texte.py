import sys
import numpy as np
import tools


class lexique:
	def __init__(self):
		self.texte = ""

	#__init__
	
	def definir_texte(self, path, enc):
		self.path = path
		self.encodage = enc
		
		self.texte += " " # éviter overlap entre deux textes
		
		f = open(path, 'r', encoding=enc)
		self.texte += f.read()
		f.close()
		
		self.texte += "." # s'assurer que la phrase est terminée
		
	def text(self):
		self.texte = tools.cleaner(self.texte)
		return self.texte