#-*- coding: utf-8 -*-
#Entrainement --> -e -t 7 -enc utf-8-sig -cc .\GerminalUTF8.txt
#Recherche --> -s -t 7

import sys
import os
import time

import le_texte
import les_phrases
import le_dictionnaire
import la_matrice
import tools as t
import calculs_scores as cs
import baseDonnees as bd

import cx_Oracle


def main():
	os.system('cls' if os.name == 'nt' else 'clear') #clear fenêtre

	#CONNEXION BD
	matricule = 'e1555391'
	mdp = 'a' #Please ne pas scraper mes données

	dsn_tns = cx_Oracle.makedsn('delta', 1521, 'decinfo')
	chaineConnexion = matricule + '/' + mdp + '@' + dsn_tns

	connexion = cx_Oracle.connect(chaineConnexion)
	cur = connexion.cursor()
	

	# Création objets
	sl = le_texte.lexique()
	ph = les_phrases.phrases()
	dico = le_dictionnaire.dictionnaire(connexion)
	ma = la_matrice.matrice(connexion)
	#TIMER START
	start = time.time()

	#traitement des arg
	if(len(sys.argv) < 1):
		print("Aucun arguments founis")

	elif(sys.argv[1] == "-e" and len(sys.argv) > 7):	#ENTRAINEMENT
		print("Entrainement")
		fdeconcu = sys.argv[3] # la fenêtre de cooccurrences
		encodage = sys.argv[5] # utf-8-sig
		chemin = []
		for i in range (7,len(sys.argv)):#loop pour trouver tout les fichiers donné en arg
			chemin.append(sys.argv[i])
			
		#imprimer info des arg donnés
		print("fenêtre de cooccurrences: ", fdeconcu)
		print("encodage : ", encodage)
		for i in range (0,len(chemin)):
			print("chemin", i+1, ": ",chemin[i])
		print("\n")

		#INSERTIONS BD phrases
		
		
		#prendre chemin + build string
		for i in range (0,len(chemin)):
			sl.definir_texte(chemin[i],encodage)
		
		#sépare en phrases -> array
		texte=ph.separer_phrases(sl.text())

		#créé dictionnaire
		dico.definir_dictionaire(ph.p(),ph.nb_phrases())

		#Créé matrice init 0
		ma.creer_matrice(dico.grandeur_dictionnaire())

		#popule la matrice
		ma.pop_matrice(ph.p(),ph.nb_phrases(), dico.d(),fdeconcu)
		
		
		#TIMER END
		end = time.time()
		print("Initialisé en: ", end - start, " secondes!")
		print("\n\n\n")

	elif(sys.argv[1] == "-s"and len(sys.argv) > 3):		#RECHERCHE
		print("Recherche")
		fdeconcu = sys.argv[3]

		#imprimer info des arg donnés
		print("fenêtre de cooccurrences: ", fdeconcu)


		#Vérification que la fenetre demande existe
		cur.execute('SELECT taille_fenetre FROM matrice')
		fc = cur.fetchall()
		fca = [row[0] for row in fc]


		#vérifier --> fdeconcu = fdeconcu dans la bd
		if int(fdeconcu) in fca:

			#importer la matrice (prendre de la bd et l'ajouter dans la matrice du programme)
			dico.definir_dico_s()
			ma.creer_matrice(dico.grandeur_dictionnaire())
			ma.reconstruction(fdeconcu, dico)

			#TIMER END
			end = time.time()
			print("Initialisé en: ", end - start, " secondes!")
			print("\n\n\n")

			try:
				#Début des recherches
				mot = None
				nbsyn = None
				funcscore = None
				
				#Input nouveau mot
				print("Entrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,")
				print("i.e. produit scalaire: 0, least squares: 1, cityblock:2")
				print("-1 pour quitter")
				mot, nbsyn, funcscore = input("input:").split()
				#vérifier arguments donné
				if funcscore not in ['0','1','2']:
					print("funcscore invalide!  ")
				mot.strip()
				
				while mot != "-1":
					print("index du mot \'", mot, "\' est: ",dico.trouver_index(mot))
					if dico.trouver_index(mot) == None:
						print("mot pas dans dictionnaire")
					else:
						#TIMER START
						start = time.time()
						if funcscore in ['0']:
							print("\nproduitscalaire: \n")

							scores = cs.ts_produitscalaire(ma.m(),dico.trouver_index(mot))
							s_tup = cs.ordonner_scores(scores,"+")
							s_tup = t.remove(s_tup,dico,dico.trouver_index(mot));
							
						elif funcscore in ['1']:
							print("\nleastsquares: \n")
							
							scores = cs.ts_leastsquares(ma.m(),dico.trouver_index(mot))
							s_tup = cs.ordonner_scores(scores,"-")
							s_tup = t.remove(s_tup,dico,dico.trouver_index(mot));
							
						else:
							print("\ncityblock: \n")
							
							scores = cs.ts_cityblock(ma.m(),dico.trouver_index(mot))
							s_tup = cs.ordonner_scores(scores,"-")
							s_tup = t.remove(s_tup,dico,dico.trouver_index(mot));
							
						if len(s_tup) < int(nbsyn):
							for i in range (0,len(s_tup)):
								print(i+1,"- ",dico.trouver_mot(s_tup[i][0]), ", score: [", s_tup[i][1], "]")
						else:
							for i in range (0,int(nbsyn)):
								print(i+1,"- ",dico.trouver_mot(s_tup[i][0]), ", score: [", s_tup[i][1], "]")
						#TIMER END
						end = time.time()
						print("\nTrouvé en: ", end - start, " secondes!")
					
					#Input nouveau mot
					print("\nEntrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul,")
					print("i.e. produit scalaire: 0, least squares: 1, cityblock:2")
					print("-1 pour quitter")
					mot, nbsyn, funcscore = input("input:").split()
					#vérifier arguments donné
					if funcscore not in ['0','1','2']:
						print("funcscore invalide!  ")
					mot.strip()
			except ValueError as e:
				if mot != "-1":
					print(e)
		else:
			print('mauvaise fenêtre de cooccurrence')
	else:
		print("Mauvais arguments")

	#Ferme la connexion
	connexion.close()

	return 0
	
if __name__=='__main__':
	sys.exit(main())