drop table matrice;

Create Table Matrice(
  id number(10),
  motCentral varchar2(24) not null,
  motConcurence varchar2(24) not null,
  taille_fenetre number(3) not null,
  
  constraint pk_matrice primary key(id)
);

Drop SEQUENCE seq_matrice;

CREATE SEQUENCE seq_matrice START WITH 1 INCREMENT BY 1;




