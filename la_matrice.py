import numpy as np
import tools
import baseDonnees as bd
import le_dictionnaire


class matrice:
	def __init__(self,connexion):
		self.cursor = connexion.cursor()
		self.matrice = [[]]
		self.connexion=connexion

		self.motsLigne = []
		self.motsColonne = []

	def creer_matrice(self, grandeur):
		self.matrice = np.zeros((grandeur,grandeur))

	def pop_matrice(self, phrases, nb_phrases, dict,fdeconcu):
		x=0
		concurence = int((int(fdeconcu)-1)/2)

		for i in range (0, nb_phrases):
			mots = phrases[i].split()
		
			for j in range (concurence , tools.countWords(phrases[i])-concurence):
				
				for x in range (1,concurence+1):
					#Retourne le mot
					self.motsLigne.append(mots[j])
					self.motsColonne.append(mots[j-x])

		bd.insertionBaseDonne(self.connexion, self.motsLigne, self.motsColonne, fdeconcu)		

	def reconstruction(self,fenetre,dico):

		self.cursor.execute("SELECT Motcentral, Motconcurence, COUNT(*) as apparition FROM ( SELECT CASE WHEN Motcentral < Motconcurence THEN Motcentral ELSE Motconcurence END AS Motcentral, CASE WHEN Motcentral < Motconcurence THEN Motconcurence ELSE Motcentral END AS Motconcurence FROM Matrice ) t GROUP BY Motcentral, Motconcurence Order by apparition")
		reponse=self.cursor.fetchall()
		for ligne in reponse:
			self.matrice[dico.trouver_index(ligne[0])][dico.trouver_index(ligne[1])]=ligne[2]

	def retourne_ligne(self, noligne):
		ligne = self.matrice[noligne]
		return ligne
			
	def m(self):
		return self.matrice
