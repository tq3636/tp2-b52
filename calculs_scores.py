import numpy as np

from operator import itemgetter, attrgetter, methodcaller

def ts_produitscalaire(matrice,noligne):
    motsimilaire = []
    ligne = matrice[noligne]
    
    motsimilaire = np.dot(matrice, ligne)
    
    return motsimilaire
    
def ts_leastsquares(matrice,noligne):
    motsimilaire = []
    ligne = matrice[noligne]
    nmatrice = [[]]
    
    for x in range(0,len(matrice)):
        nmatrice = np.square(np.subtract(matrice[x], ligne))
        nmatrice = nmatrice.sum(axis=0)
        motsimilaire.append(nmatrice)

    return motsimilaire
        
def ts_cityblock(matrice,noligne):
    motsimilaire = []
    ligne = matrice[noligne]
    nmatrice = [[]]
    
    for x in range(0,len(matrice)):
        nmatrice = np.absolute(np.subtract(matrice[x], ligne))
        nmatrice = nmatrice.sum(axis=0)
        motsimilaire.append(nmatrice)

    return motsimilaire
    return motsimilaire

def ordonner_scores(scores,force):
    tup=[]
    for i in range (0,len(scores)):
        tup.append((i,scores[i]))
        
    if force == "+":
        return sorted(tup, key=itemgetter(1), reverse=True)
    else:
        return sorted(tup, key=itemgetter(1), reverse=False)